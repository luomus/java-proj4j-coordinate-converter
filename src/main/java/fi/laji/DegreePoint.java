package fi.laji;

public class DegreePoint {

	private final Double lat;
	private final Double lon;

	public DegreePoint(double lat, double lon) {
		this.lat = lat;
		this.lon = lon;
	}

	private DegreePoint() {
		this.lat = null;
		this.lon = null;
	}

	public DegreePoint emptyPoint() {
		return new DegreePoint();
	}

	public Double getLat() {
		return lat;
	}

	public Double getLon() {
		return lon;
	}

	@Override
	public String toString() {
		return "DegreePoint [lat=" + lat + ", lon=" + lon + "]";
	}

}
