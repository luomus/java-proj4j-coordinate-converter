package fi.laji;

public interface CoordinateConverter {

	public static final int EUREF_NORTHING_MIN = 6543360;
	public static final int EUREF_EASTING_MIN  =  -44768;
	public static final int EUREF_NORTHING_MAX = 7880704;
	public static final int EUREF_EASTING_MAX  = 1042720;
	
	public Conversion convertFromYKJ(MetricPoint ykj);
	public Conversion convertFromEuref(MetricPoint euref);
	public Conversion convertFromWGS84(DegreePoint wgs84);
	
}
