package fi.laji;

import org.osgeo.proj4j.CRSFactory;
import org.osgeo.proj4j.CoordinateReferenceSystem;
import org.osgeo.proj4j.CoordinateTransformFactory;
import org.osgeo.proj4j.ProjCoordinate;

public class CoordinateConverterProj4jImple implements CoordinateConverter {

	private static final CoordinateReferenceSystem CRS_WGS84 = new CRSFactory().createFromName("epsg:4326");
	private static final CoordinateReferenceSystem CRS_EUREF = new CRSFactory().createFromName("epsg:3067");
	private static final CoordinateReferenceSystem CRS_YKJ = new CRSFactory().createFromName("epsg:2393");

	@Override
	public Conversion convertFromYKJ(MetricPoint ykj) {
		ProjCoordinate source = new ProjCoordinate(toFullYkj(ykj.getEasting()), toFullYkj(ykj.getNorthing()));
		ProjCoordinate euref = convert(source, CRS_YKJ, CRS_EUREF);
		ProjCoordinate wgs84 = convert(source, CRS_YKJ, CRS_WGS84);
		return conversion(wgs84, euref, source);
	}

	private int toFullYkj(Integer i) {
		String s = i.toString();
		while (s.length() < 7) {
			s += "0";
		}
		return Integer.valueOf(s);
	}

	@Override
	public Conversion convertFromEuref(MetricPoint euref) {
		if (outsideBounds(euref)) {
			throw new IllegalArgumentException("When converting using EUREF coordinates, they must be given in full form. The given value is outside range.");
		}
		ProjCoordinate source = new ProjCoordinate(euref.getEasting(), euref.getNorthing());
		ProjCoordinate ykj = convert(source, CRS_EUREF, CRS_YKJ);
		ProjCoordinate wgs84 = convert(source, CRS_EUREF, CRS_WGS84);
		return conversion(wgs84, source, ykj);
	}

	@Override
	public Conversion convertFromWGS84(DegreePoint wgs84) {
		ProjCoordinate source = new ProjCoordinate(wgs84.getLon(), wgs84.getLat());
		ProjCoordinate ykj = convert(source, CRS_WGS84, CRS_YKJ);
		ProjCoordinate euref = convert(source, CRS_WGS84, CRS_EUREF);
		return conversion(source, euref, ykj);
	}

	private ProjCoordinate convert(ProjCoordinate source, CoordinateReferenceSystem sourceCRS, CoordinateReferenceSystem targetCRS) {
		return new CoordinateTransformFactory().createTransform(sourceCRS, targetCRS).transform(source, new ProjCoordinate());
	}

	private Conversion conversion(ProjCoordinate wgs84, ProjCoordinate euref, ProjCoordinate ykj) {
		DegreePoint wgs84Point = new DegreePoint(wgs84.y, wgs84.x);
		MetricPoint eurefPoint = new MetricPoint(euref.y, euref.x);
		MetricPoint ykjPoint = new MetricPoint(ykj.y, ykj.x);
		if (outsideBounds(eurefPoint)) {
			return new Conversion(wgs84Point, MetricPoint.emptyPoint(), MetricPoint.emptyPoint());
		}
		return new Conversion(wgs84Point, eurefPoint, ykjPoint);
	}

	private boolean outsideBounds(MetricPoint eurefPoint) {
		return eurefPoint.getNorthing() > EUREF_NORTHING_MAX || 
				eurefPoint.getNorthing() < EUREF_NORTHING_MIN || 
				eurefPoint.getEasting() > EUREF_EASTING_MAX || 
				eurefPoint.getEasting() < EUREF_EASTING_MIN;
	}

}
