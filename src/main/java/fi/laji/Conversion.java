package fi.laji;

public class Conversion {

	private final DegreePoint wgs84;
	private final MetricPoint euref;
	private final MetricPoint ykj;

	public Conversion(DegreePoint wgs84, MetricPoint euref, MetricPoint ykj) {
		this.wgs84 = wgs84;
		this.euref = euref;
		this.ykj = ykj;
	}

	public DegreePoint getWgs84() {
		return wgs84;
	}

	public MetricPoint getEuref() {
		return euref;
	}

	public MetricPoint getYkj() {
		return ykj;
	}

	@Override
	public String toString() {
		return "Conversion [wgs84=" + wgs84 + ", euref=" + euref + ", ykj=" + ykj + "]";
	}
	
}
