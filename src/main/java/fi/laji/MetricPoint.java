package fi.laji;

public class MetricPoint {

	private final Integer northing;
	private final Integer easting;

	public MetricPoint(int northing, int easting) {
		this.northing = northing;
		this.easting = easting;
	}

	public MetricPoint(double northing, double easting) {
		this.northing = (int) Math.round(northing);
		this.easting = (int) Math.round(easting);
	}

	private MetricPoint() {
		this.northing = null;
		this.easting = null;
	}

	public static MetricPoint emptyPoint() {
		return new MetricPoint();
	}

	public Integer getNorthing() {
		return northing;
	}

	public Integer getEasting() {
		return easting;
	}

	public boolean isEmpty() {
		return northing == null || easting == null;
	}

	@Override
	public String toString() {
		return "MetricPoint [northing=" + northing + ", easting=" + easting + "]";
	}

}
