
package org.osgeo.proj4j;

/**
 * Signals that an interative mathematical algorithm has failed to converge.
 * This is usually due to values exceeding the
 * allowable bounds for the computation in which they are being used.
 * 
 * @author mbdavis
 *
 */
public class ConvergenceFailureException extends Proj4jException 
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 3627518170047670855L;

	public ConvergenceFailureException() {
		super();
	}

	public ConvergenceFailureException(String message) {
		super(message);
	}
}
