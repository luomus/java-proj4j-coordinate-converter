package fi.laji;

import java.util.Random;

import junit.framework.TestCase;
import junit.textui.TestRunner;

public class FinnishCoordinateSystemsTests extends TestCase
{
	private static final double WGS84_ALLOWED_ERROR = 0.00005;

	public static void main(String args[]) {
		TestRunner.run(FinnishCoordinateSystemsTests.class);
	}

	public void test_from_wgs84_inside_finland() {
		double lat = 63.06153447; // middle of finland
		double lon = 26.653125962;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(6992493, conversion.getEuref().getNorthing().intValue());
		assertEquals(482467, conversion.getEuref().getEasting().intValue());

		assertEquals(6995423, conversion.getYkj().getNorthing().intValue());
		assertEquals(3482631, conversion.getYkj().getEasting().intValue());

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_wgs84_inside_finland_2() {
		double lat = 70.017941028; // north border
		double lon = 27.84460414;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(7768097, conversion.getEuref().getNorthing().intValue());
		assertEquals(532211, conversion.getEuref().getEasting().intValue());

		assertEquals(7771339, conversion.getYkj().getNorthing().intValue()); // difference with karttapaikka: 1 meter 
		assertEquals(3532397, conversion.getYkj().getEasting().intValue()); // difference with karttapaikka: 2 meters

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_wgs84_inside_finland_3() {
		double lat = 60.198164508; // ahvenanmaa
		double lon = 19.334297616;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(6698170, conversion.getEuref().getNorthing().intValue());
		assertEquals(75628, conversion.getEuref().getEasting().intValue());

		assertEquals(6700984, conversion.getYkj().getNorthing().intValue());
		assertEquals(3075628, conversion.getYkj().getEasting().intValue()); // difference with karttapaikka: 2 meters

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_wgs84_outside_finland_1() {
		double lat = 60.878582541; // laatokka
		double lon = 31.456221847;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(6757484, conversion.getEuref().getNorthing().intValue());
		assertEquals(741809, conversion.getEuref().getEasting().intValue());

		assertEquals(6760319, conversion.getYkj().getNorthing().intValue());
		assertEquals(3742077, conversion.getYkj().getEasting().intValue());

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_wgs84_outside_finland_2() {
		double lat = 59.372405556; // stockholmn
		double lon = 17.982726667;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(6616261, conversion.getEuref().getNorthing().intValue());
		assertEquals(-11435, conversion.getEuref().getEasting().intValue());
		assertFalse(conversion.getEuref().isEmpty());

		assertEquals(6619042, conversion.getYkj().getNorthing().intValue());
		assertEquals(2988529, conversion.getYkj().getEasting().intValue());
		assertFalse(conversion.getYkj().isEmpty());

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_wgs84_far_outside_finland() {
		double lat = 0;
		double lon = 0;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromWGS84(new DegreePoint(lat, lon));

		assertEquals(null, conversion.getEuref().getNorthing());
		assertEquals(null, conversion.getEuref().getEasting());
		assertTrue(conversion.getEuref().isEmpty());

		assertEquals(null, conversion.getYkj().getNorthing());
		assertEquals(null, conversion.getYkj().getEasting());
		assertTrue(conversion.getYkj().isEmpty());

		assertEquals(lat, conversion.getWgs84().getLat(), 0);
		assertEquals(lon, conversion.getWgs84().getLon(), 0);
	}

	public void test_from_euref_inside_finland() {
		int n = 6811469;
		int e = 473397;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromEuref(new MetricPoint(n, e));

		assertEquals(61.436139494, conversion.getWgs84().getLat(), WGS84_ALLOWED_ERROR);
		assertEquals(26.501292981, conversion.getWgs84().getLon(), WGS84_ALLOWED_ERROR);

		assertEquals(6814327, conversion.getYkj().getNorthing().intValue());
		assertEquals(3473556, conversion.getYkj().getEasting().intValue());

		assertEquals(n, conversion.getEuref().getNorthing().intValue());
		assertEquals(e, conversion.getEuref().getEasting().intValue());
	}

	public void test_from_euref_outside_finland() {
		int n = 7325369; // Töre, Sweden
		int e = 300106;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromEuref(new MetricPoint(n, e));

		assertEquals(65.985969634, conversion.getWgs84().getLat(), WGS84_ALLOWED_ERROR);
		assertEquals(22.595266075, conversion.getWgs84().getLon(), WGS84_ALLOWED_ERROR);

		assertEquals(7328434, conversion.getYkj().getNorthing().intValue());
		assertEquals(3300198, conversion.getYkj().getEasting().intValue());

		assertEquals(n, conversion.getEuref().getNorthing().intValue());
		assertEquals(e, conversion.getEuref().getEasting().intValue());
	}

	public void test_from_euref_faroutside_finland() {
		int n = 6447104; // Novgorod, Russia
		int e = 734291;

		try {
			new CoordinateConverterProj4jImple().convertFromEuref(new MetricPoint(n, e));
			fail("should throw exception");
		} catch (IllegalArgumentException ex) {
			assertEquals("When converting using EUREF coordinates, they must be given in full form. The given value is outside range.", ex.getMessage());
		}
	}

	public void test_from_ykj_inside_finland() {
		int n = 7122701;
		int e = 3665742;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromYKJ(new MetricPoint(n, e));

		assertEquals(64.164028333, conversion.getWgs84().getLat(), WGS84_ALLOWED_ERROR);
		assertEquals(30.404901389, conversion.getWgs84().getLon(), WGS84_ALLOWED_ERROR);

		assertEquals(7119720, conversion.getEuref().getNorthing().intValue());
		assertEquals(665504, conversion.getEuref().getEasting().intValue());

		assertEquals(n, conversion.getYkj().getNorthing().intValue());
		assertEquals(e, conversion.getYkj().getEasting().intValue());
	}

	public void test_from_ykj_outside_finland() {
		int n = 6900878; // Karjala
		int e = 3775397;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromYKJ(new MetricPoint(n, e));

		assertEquals(62.113103307, conversion.getWgs84().getLat(), WGS84_ALLOWED_ERROR);
		assertEquals(32.276324806, conversion.getWgs84().getLon(), WGS84_ALLOWED_ERROR);

		assertEquals(6897987, conversion.getEuref().getNorthing().intValue());
		assertEquals(775116, conversion.getEuref().getEasting().intValue());

		assertEquals(n, conversion.getYkj().getNorthing().intValue());
		assertEquals(e, conversion.getYkj().getEasting().intValue());
	}

	public void test_from_ykj_far_outside_finland() {
		int n = 7591460; // Snopa, Siperia
		int e = 4395169;

		Conversion conversion = new CoordinateConverterProj4jImple().convertFromYKJ(new MetricPoint(n, e));

		assertEquals(67.05234296, conversion.getWgs84().getLat(), 0.1);
		assertEquals(47.79578365, conversion.getWgs84().getLon(), 0.1);

		assertEquals(null, conversion.getEuref().getNorthing());
		assertEquals(null, conversion.getEuref().getEasting());
		assertTrue(conversion.getEuref().isEmpty());

		assertEquals(null, conversion.getYkj().getNorthing());
		assertEquals(null, conversion.getYkj().getEasting());
		assertTrue(conversion.getYkj().isEmpty());
	}

	public void test_from_ykj_inside_finland_shortform() {
		Conversion conversion = new CoordinateConverterProj4jImple().convertFromYKJ(new MetricPoint(666, 333));

		assertEquals(6657204, conversion.getEuref().getNorthing().intValue());
		assertEquals(329899, conversion.getEuref().getEasting().intValue());

		assertEquals(60.016767429, conversion.getWgs84().getLat(), WGS84_ALLOWED_ERROR);
		assertEquals(23.94812116, conversion.getWgs84().getLon(), WGS84_ALLOWED_ERROR);

		assertEquals(6660000, conversion.getYkj().getNorthing().intValue());
		assertEquals(3330000, conversion.getYkj().getEasting().intValue());
	}

	public void test_from_euref_inside_finland_shortform() {
		try {
			new CoordinateConverterProj4jImple().convertFromEuref(new MetricPoint(666, 333));
			fail("should throw exception");
		} catch (IllegalArgumentException e) {
			assertEquals("When converting using EUREF coordinates, they must be given in full form. The given value is outside range.", e.getMessage());
		}
	}

	public void test_performance() {
		CoordinateConverter converter = new CoordinateConverterProj4jImple();
		int count = 1000000;
		while (count-- > 0) {
			Conversion c = generateTest(converter);
			if (count % 1000 == 0) {
				System.out.println(c.toString());
			}
		}
	}

	private Conversion generateTest(CoordinateConverter converter) {
		double randomLat = getRandom(58, 71);
		double randomLon = getRandom(16, 40);
		return converter.convertFromWGS84(new DegreePoint(randomLat, randomLon));
	}

	private double getRandom(double min, double max) {
		Random r = new Random();
		double randomValue = min + (max - min) * r.nextDouble();
		return randomValue;
	}

}
